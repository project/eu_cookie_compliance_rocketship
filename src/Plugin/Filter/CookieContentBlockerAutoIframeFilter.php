<?php

namespace Drupal\eu_cookie_compliance_rocketship\Plugin\Filter;

use function preg_match_all;
use function str_replace;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Text filter that wraps iframes into <cookiecontentblocker> tags.
 *
 * @Filter(
 *   id = "cookie_content_blocker_filter_auto_iframe",
 *   title = @Translation("Cookie content blocker filter - autowrap iframes"),
 *   description = @Translation("This filter automatically wraps &lt;iframe&gt; tags into Cookie content blocker's custom &lt;cookiecontentblocker&gt; tags. <br/><i>Note:</i> It <strong>requires</strong> and it must run <strong>before</strong> the 'Cookie content blocker filter'."),
 *   type = \Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class CookieContentBlockerAutoIframeFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Construct a CookieContentBlockerFilter plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): CookieContentBlockerAutoIframeFilter {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $result = new FilterProcessResult($text);
    if (empty($text)) {
      return $result;
    }

    return $result->setProcessedText($this->replaceTags($text));
  }

  /**
   * Replaces iframe tags for the given text.
   *
   * @param string $text
   *   The HTML/Text where we want to replace tags for.
   *
   * @return string
   *   The text with tags replaced.
   */
  private function replaceTags(string $text): string {
    $matches = $this->matchTags($text);

    foreach ($matches as $index => $match) {
      $blocked_content_element = [
        // We depend on other filters to have sanitized the content.
        '#markup' => Markup::create('<cookiecontentblocker>' . $match . '</cookiecontentblocker>'),
      ];

      $text = str_replace($match, $this->renderer->renderPlain($blocked_content_element), $text);
    }

    return $text;
  }

  /**
   * Match iframes nat yet wrapped in our own custom HTML element node '<cookiecontentblocker>'.
   *
   * @param string $text
   *   The HTML/Text string.
   *
   * @return array
   *   An array containing full pattern matches.
   */
  private function matchTags(string $text): array {
    preg_match_all('/(?<!<cookiecontentblocker>)<iframe.*?>.*?<\/iframe>(?!<\/cookiecontentblocker>)/s', $text, $matches);
    return $matches[0];
  }

}
