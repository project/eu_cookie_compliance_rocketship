<?php

namespace Drupal\eu_cookie_compliance_rocketship\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\iframe\Plugin\Field\FieldFormatter\IframeDefaultFormatter;

class CookieBlockedIframe extends IframeDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // Let parent formatter do it's magic.
    $elements = parent::viewElements($items, $langcode);

    // Attach Cookie Content Blocker's pre_render.
    foreach ($items as $delta => $item) {
      $element[$delta]['#pre_render'] = $element[$delta]['#pre_render'] ?? [];
      $elements[$delta]['#pre_render'][] = 'cookie_content_blocker.element.processor:processElement';
      // TODO: extend formatter settings with CCB options (like button_text) and set them here.
      //   $defaults = [
      //     'blocked_message' => $config->get('blocked_message'),
      //     'show_button' => $config->get('show_button'),
      //     'button_text' => $config->get('button_text'),
      //     'enable_click' => $config->get('enable_click_consent_change'),
      //     'show_placeholder' => TRUE,
      //     'preview' => [],
      //   ];
      $elements[$delta]['#cookie_content_blocker'] = $elements[$delta]['#cookie_content_blocker'] ?? TRUE;
    }

    return $elements;
  }

}
