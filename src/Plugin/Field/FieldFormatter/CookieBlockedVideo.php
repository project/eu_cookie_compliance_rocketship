<?php

namespace Drupal\eu_cookie_compliance_rocketship\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\video_embed_field\Plugin\Field\FieldFormatter\Video;

class CookieBlockedVideo extends Video {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Let parent formatter do it's magic.
    $element = parent::viewElements($items, $langcode);

    // Attach Cookie Content Blocker's pre_render.
    foreach ($items as $delta => $item) {
      $provider = $this->providerManager->loadProviderFromInput($item->value);

      if ($provider) {
        $element[$delta]['#pre_render'] = $element[$delta]['#pre_render'] ?? [];
        $element[$delta]['#pre_render'][] = 'cookie_content_blocker.element.processor:processElement';
        // TODO: extend formatter settings with CCB options (like button_text) and set them here.
        //   $defaults = [
        //     'blocked_message' => $config->get('blocked_message'),
        //     'show_button' => $config->get('show_button'),
        //     'button_text' => $config->get('button_text'),
        //     'enable_click' => $config->get('enable_click_consent_change'),
        //     'show_placeholder' => TRUE,
        //     'preview' => [],
        //   ];
        $element[$delta]['#cookie_content_blocker'] = $element[$delta]['#cookie_content_blocker'] ?? TRUE;
      }
    }

    return $element;
  }

}
