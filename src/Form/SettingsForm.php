<?php

/**
 * @file
 * Contains Drupal\eu_cookie_compliance_rocketship\Form\SettingsForm.
 */

namespace Drupal\eu_cookie_compliance_rocketship\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\eu_cookie_compliance_rocketship\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'eu_cookie_compliance_rocketship.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eu_cookie_compliance_rocketship_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('eu_cookie_compliance_rocketship.settings');

    $form['eu_cookie_compliance_rocketship_styling_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Styling settings'),
      '#open' => TRUE,
    ];

    $form['eu_cookie_compliance_rocketship_styling_settings']['css_structural'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable default CSS'),
      '#default_value' => $config->get('css_structural'),
      '#description' => $this->t('Loads a CSS file with some very basic CSS included in the module.'),
    ];

    $form['eu_cookie_compliance_rocketship_styling_settings']['css_extra'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable extra CSS'),
      '#default_value' => $config->get('css_extra'),
      '#description' => $this->t('Loads a CSS file with extra CSS included in the module.'),
    ];

    $form['eu_cookie_compliance_rocketship_language_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Language settings'),
      '#open' => TRUE,
    ];

    $form['eu_cookie_compliance_rocketship_language_settings']['language_switcher'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show language switcher'),
      '#default_value' => $config->get('language_switcher'),
      '#description' => $this->t('Show a language switcher inside the popup.'),
    ];

    $form['eu_cookie_compliance_rocketship_button_labels'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Button labels'),
      '#open' => TRUE,
    ];

    $form['eu_cookie_compliance_rocketship_button_labels']['accept_all_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Accept all" button label'),
      '#default_value' => $config->get('accept_all_label'),
      '#description' => $this->t('The label of the "Accept all" button.'),
    ];

    $form['eu_cookie_compliance_rocketship_button_labels']['manage_categories_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Manage cookies" button label'),
      '#default_value' => $config->get('manage_categories_label'),
      '#description' => $this->t('The label of the "Accept all" button.'),
    ];

    $form['eu_cookie_compliance_rocketship_button_labels']['accept_selection_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Save preferences" button label'),
      '#default_value' => $config->get('accept_selection_label'),
      '#description' => $this->t('The label of the "Accept all" button.'),
    ];

    $form['eu_cookie_compliance_rocketship_button_labels']['accept_minimal_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Accept necessary only" button label'),
      '#default_value' => $config->get('accept_minimal_label'),
      '#description' => $this->t('The label of the "Accept necessary only" button.'),
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Save the values.
    $config = $this->config('eu_cookie_compliance_rocketship.settings');
    $config->set('css_structural', $form_state->getValue('css_structural'))
      ->set('css_extra', $form_state->getValue('css_extra'))
      ->set('language_switcher', $form_state->getValue('language_switcher'))
      ->set('accept_all_label', $form_state->getValue('accept_all_label'))
      ->set('manage_categories_label', $form_state->getValue('manage_categories_label'))
      ->set('accept_selection_label', $form_state->getValue('accept_selection_label'))
      ->set('accept_minimal_label', $form_state->getValue('accept_minimal_label'))
      ->save();

    parent::submitForm($form, $form_state);

    Cache::invalidateTags(['eu_cookie_compliance_rocketship:attachments']);

    // Also invalidate eu_cookie_compliance settings as language switcher is cached as rendered string inside variables.
    Cache::invalidateTags(\Drupal::config('eu_cookie_compliance.settings')->getCacheTags());
  }

}
