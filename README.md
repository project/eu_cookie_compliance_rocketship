# Eu cookie compliance rocketship

An extension module containing rocketship specific functionalities for the eu_cookie_compliance module

## Specific functionalities
* Allows reopening of the info popup via any link with the "#eucc-open" fragment
* Hides the default EUCC buttons for a dynamic accept link (accept all or save preferences) and a accept only required link
* Adds language links to the info popup
* Changes the category checkboxes to toggles

## Required configuration
This module requires the following EUCC configuration:
* Opt-in with categories
    * With replacement of the agree button
* Disabled withdraw consent and thank you banners
* Top positioned info popup
* Minimal CSS
