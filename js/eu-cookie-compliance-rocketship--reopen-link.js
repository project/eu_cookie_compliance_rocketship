/*global Drupal, jQuery*/
Drupal.behaviors.euccRocketshipReopenLink = {
  attach: function (context, settings) {
    (function ($, once) {

      function show_eucc_popup() {
        var popup = $('#sliding-popup');
        if (!popup.length) {
          if (window.matchMedia('(max-width: ' + settings.eu_cookie_compliance.mobile_breakpoint + 'px)').matches
              && settings.eu_cookie_compliance.use_mobile_message) {
            Drupal.eu_cookie_compliance.createPopup(settings.eu_cookie_compliance.mobile_popup_html_info, false);
          } else {
            Drupal.eu_cookie_compliance.createPopup(settings.eu_cookie_compliance.popup_html_info, false);
          }
          Drupal.eu_cookie_compliance.initPopup();
        }
        else {
          popup.css({ top: 0 });
          popup.trigger('eu_cookie_compliance_popup_open');
          $('body').addClass('eu-cookie-compliance-popup-open');
        }
      }

      // Init when the popup has opened
      once('euccRocketshipReopenLink', 'body').forEach(function(element) {
        var links = $('a[href$="#eucc-open"]');
        links.click(function (e) {
          e.preventDefault();
          show_eucc_popup();
        });
      });

      // Handle Cookie Content Blocker's event to open the popup.
      $(window).on('cookieContentBlockerChangeConsent', function () {
        show_eucc_popup();
      });

    }(jQuery, once));
  }
};
