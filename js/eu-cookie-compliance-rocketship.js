/*global Drupal, jQuery*/
Drupal.behaviors.euccRocketship = {
  attach: function (context, settings) {
    (function ($) {

      // Init when the popup has opened
      once('euccRocketshipOpen', 'body').forEach(function (element) {
        $(element).on('eu_cookie_compliance_popup_open', function() {
          once('euccRocketship', '.eu-cookie-compliance-banner-info').forEach(function (element) {
            initCloseEvent();
            initStateLabel();
            initAcceptMinimal();
            initAcceptAll();
            initManageSelection();
            initAcceptSelection();
          });

          // Initiate accessibility for the modal.
          initAccessibility();
        });
      });

      /**
       * Helper function to trigger an event when the popup is closed
       */
      function initCloseEvent() {
        once('euccRocketshipCloseEvent', '.eu-cookie-compliance-banner-info button').forEach(function (element) {
          $(element).click(function (e) {
            $(this).trigger('eu_cookie_compliance_rocketship__popup_close')
            Drupal.attachBehaviors(document.getElementById('sliding-popup'), settings);
          });
        })
      }

      /**
       * Helper function to initialize the state label functionality
       */
      function initStateLabel() {
        var categories = $('.eu-cookie-compliance-category');
        categories.each(function () {
          var category = $(this);
          var check = category.find('input[type="checkbox"]');
          var stateLabel = category.find('.state-label');
          var aria = category.find('.toggle');
          check.change(function () {
            if (check.prop('checked')) {
              stateLabel.addClass('visible');
              aria.attr('aria-checked','true')
            }
            else {
              stateLabel.removeClass('visible');
              aria.attr('aria-checked','false')
            }
          });
        });
      }

      /**
       * Helper function to initialize the manage cookie button
       */
      function initManageSelection() {
        var manageSelectionButton = $('.eu-cookie-compliance-rocketship--manage-selection');
        var acceptSelectionButton = $('.eu-cookie-compliance-rocketship--accept-selection');
        var acceptAllButton = $('.eu-cookie-compliance-rocketship--accept-all');
        var categoryWrapper = $('.eu-cookie-compliance-categories');
        manageSelectionButton.click(function (e) {
          e.preventDefault();
          manageSelectionButton.parent().hide();
          acceptSelectionButton.parent().show();
          categoryWrapper.show();
          // Remove tabindex from hidden inputs - we use the labels already.
          categoryWrapper.find('input').attr('tabindex', "-1");
          // When on the last button, go back to first focusable item of the modal.
          acceptAllButton.on('keydown', function (e) {
            if ($("this:focus") && (e.which === 9) && (!e.shiftKey)) {
              e.preventDefault();
              // Elements that should be reachable inside the cookie modal.
              let focusable = $('.eu-cookie-compliance-content').find('a, .eu-cookie-compliance-category:not(".disabled") .eu-cookie-compliance-category-switch');
              focusable.first().focus();
            }
          });
        });
      }

      /**
       * Helper function to initialize the accept selection button
       */
      function initAcceptSelection() {
        var acceptSelectionButton = $('.eu-cookie-compliance-rocketship--accept-selection');
        acceptSelectionButton.click(function (e) {
          e.preventDefault();
          $('.eu-cookie-compliance-save-preferences-button').click();
        });
      }

      /**
       * Helper function to initialize the accept all link.
       */
      function initAcceptAll() {
        var acceptAllLink = $('.eu-cookie-compliance-rocketship--accept-all');
        acceptAllLink.click(function (e) {
          e.preventDefault();
          $('.agree-button.eu-cookie-compliance-default-button').click();
        });
      }

      /**
       * Helper function to initialize the accept minimal link.
       */
      function initAcceptMinimal() {
        var acceptMinimalLink = $('.eu-cookie-compliance-rocketship--accept-minimal');
        var categoryChecks = $('.eu-cookie-compliance-category:not(.disabled) input[type="checkbox"]');
        acceptMinimalLink.click(function (e) {
          e.preventDefault();
          categoryChecks.prop('checked', false);
          $('.eu-cookie-compliance-save-preferences-button').click();
        });
      }

      /**
       * Helper function to initialize accessibility features.
       */
      function initAccessibility() {
        // Elements that should not reachable via sequential keyboard navigation.
        let background = $('body > :not("#sliding-popup")');
        // Elements that should be reachable inside the cookie modal.
        let focusable = $('.eu-cookie-compliance-content').find('a, .eu-cookie-compliance-category:not(".disabled") .eu-cookie-compliance-category-switch');

        // Hide content from screen readers.
        background.attr('aria-hidden',true);
        // Focus the first focusable element in the modal.
        focusable.first().focus();

        // When any access button is clicked.
        $('.eu-cookie-compliance-rocketship--buttons a:not(".eu-cookie-compliance-rocketship--manage-selection")').on('click', function(e) {
          // Restore access for screen readers to the main content.
          background.attr('aria-hidden','false');
          // Focus the first link in the main content.
          background.find('a:not(".skip-link")').first().focus();
        });

        // When manage button is clicked.
        $('.eu-cookie-compliance-rocketship--buttons a.eu-cookie-compliance-rocketship--manage-selection').on('click', function(e) {
          // Move focus to the first category.
          focusable.filter('.eu-cookie-compliance-category-switch').first().focus();
        });

        // Prevent tabbing on background elements.
        background.find('a, img').on('keydown', function(e) {
          if ($('body').hasClass('eu-cookie-compliance-popup-open')) {
            if (e.keyCode === 9) {
              e.preventDefault();
            }
          }
        });

        // Go back to first focusable item of the modal.
        $('.eu-cookie-compliance-rocketship--manage-selection').on('keydown', function (e) {
          if ($("this:focus") && (e.which === 9) && (!e.shiftKey)) {
            e.preventDefault();
            focusable.first().focus();
          }
        });

        // Go to last focusable item of the modal.
        focusable.first().on('keydown',function (e) {
          if ($("this:focus") && (e.which === 9) && (e.shiftKey)) {
            e.preventDefault();
            focusable.last().focus();
          }
        });

        // Let users switch the categories with space.
        let categories = $('body').find(".eu-cookie-compliance-category");
        categories.each(function(e) {
          let category = $(this);
          // When checkbox is not disabled, you can switch state.
          if (!category.hasClass('disabled')) {
            let toggle = category.find('[role="checkbox"]');
            toggle.keyup(function (e) {
              // When hit spacebar change state.
              if (e.which === 32) {
                let input = category.find('input');
                // Toggle state.
                input.prop('checked',!input.prop("checked"));
              }
            });
          }
        });
      }
    }(jQuery));
  }
};
