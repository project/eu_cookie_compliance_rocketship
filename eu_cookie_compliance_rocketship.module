<?php

/**
 * @file
 * Contains eu_cookie_compliance_rocketship.module.
 */

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Implements hook_theme_registry_alter().
 */
function eu_cookie_compliance_rocketship_theme_registry_alter(&$theme_registry) {
  $module_path = \Drupal::service('extension.list.module')->getPath('eu_cookie_compliance_rocketship');
  $theme_registry['eu_cookie_compliance_popup_info']['path'] = $module_path . '/templates';
  $theme_registry['eu_cookie_compliance_popup_info']['theme path'] = $module_path;
  $theme_registry['eu_cookie_compliance_popup_info']['variables']['lang_switcher'] = NULL;
  $theme_registry['eu_cookie_compliance_popup_info']['variables']['accept_minimal'] = NULL;
  $theme_registry['eu_cookie_compliance_popup_info']['variables']['dynamic_accept'] = NULL;
  $theme_registry['eu_cookie_compliance_popup_info']['variables']['state_on'] = NULL;
}

/**
 * Implements template_preprocess_HOOK().
 */
function eu_cookie_compliance_rocketship_preprocess_eu_cookie_compliance_popup_info(&$variables) {
  $config = \Drupal::config('eu_cookie_compliance_rocketship.settings');
  if ($config->get('language_switcher')) {
    // Add the language switcher block.
    /** @var |Drupal|Core|Path|PathMatcherInterface $pathMatcher */
    $pathMatcher = Drupal::service('path.matcher');
    $routeName = $pathMatcher->isFrontPage() ? '<front>' : '<current>';
    $url = Url::fromRoute($routeName);
    $languages = Drupal::languageManager()->getLanguages();
    $currentLanguage = Drupal::languageManager()->getCurrentLanguage()->getId();
    $disabledLanguages = [];
    if (Drupal::moduleHandler()->moduleExists('disable_language')) {
      /** @var \Drupal\disable_language\DisableLanguageManager $disableLanguageManager */
      $disableLanguageManager = Drupal::service('disable_language.disable_language_manager');
      $disabledLanguages = $disableLanguageManager->getDisabledLanguages();
    }
    $links = [];
    /** @var |Drupal|Core|Language|LanguageInterface $language */
    foreach ($languages as $langcode => $language) {
      if (empty($disabledLanguages) || !isset($disabledLanguages[$langcode])) {
        $classes = ($langcode == $currentLanguage) ? ['language-link', 'active'] : ['language-link'];
        $links[$langcode] = [
          'url' => clone $url,
          'title' => $langcode,
          'language' => $language,
          'attributes' => ['class' => $classes],
          'query' => Drupal::request()->query->all(),
        ];
      }
    }
    if (count($links) > 1) {
      $switch_links = [
        '#theme' => 'links',
        '#links' => $links,
        '#attributes' => [
          'class' =>
            [
              'eu-cookie-compliance-language-switch',
            ],
        ],
      ];
      $variables['lang_switcher'] = Drupal::service('renderer')->render($switch_links);
    }
  }

  // Add the custom rocketship buttons.
  $variables['accept_all'] = Link::createFromRoute($config->get('accept_all_label'), '<none>', [], [
    'fragment' => '',
    'external' => TRUE,
    'attributes' => [
      'class' => ['eu-cookie-compliance-rocketship--accept-all', 'button'],
      'data-all' => t('Accept all cookies'),
      'data-selection' => t('Save my preferences'),
    ],
  ])->toString();
  $variables['manage_selection'] = Link::createFromRoute($config->get('manage_categories_label'), '<none>', [], [
    'fragment' => '',
    'external' => TRUE,
    'attributes' => [
      'class' => ['eu-cookie-compliance-rocketship--manage-selection', 'button'],
    ],
  ])->toString();
  $variables['accept_selection'] = Link::createFromRoute($config->get('accept_selection_label'), '<none>', [], [
    'fragment' => '',
    'external' => TRUE,
    'attributes' => [
      'class' => ['eu-cookie-compliance-rocketship--accept-selection', 'button'],
    ],
  ])->toString();
  $variables['accept_minimal'] = Link::createFromRoute($config->get('accept_minimal_label'), '<none>', [], [
    'fragment' => '',
    'external' => TRUE,
    'attributes' => ['class' => ['eu-cookie-compliance-rocketship--accept-minimal', 'button']],
  ])->toString();

  // Add the 'on' state label.
  $variables['state_on'] = t('On', [], ['context' => 'enabled']);

  // This would be normal, but eu_cookie_compliance renders the whole thing with its own cache tag hard-coded.
  $variables['#cache']['tags'] = $config->getCacheTags();
}

/**
 * Implements hook_page_attachments_alter().
 */
function eu_cookie_compliance_rocketship_page_attachments_alter(&$variables) {
  $config = \Drupal::config('eu_cookie_compliance_rocketship.settings');
  $cssStructural = $config->get('css_structural');
  $cssExtra = $config->get('css_extra');

  if (isset($variables['#attached']['drupalSettings']['eu_cookie_compliance'])) {
    $variables['#attached']['library'][] = 'eu_cookie_compliance_rocketship/general';
    $variables['#attached']['library'][] = 'eu_cookie_compliance_rocketship/reopen_link';
    if ($cssStructural) {
      $variables['#attached']['library'][] = 'eu_cookie_compliance_rocketship/css_structural';
    }
    if ($cssExtra) {
      $variables['#attached']['library'][] = 'eu_cookie_compliance_rocketship/css_extra';
    }
    $variables['#cache']['tags'][] = 'eu_cookie_compliance_rocketship:attachments';
  }
}

/**
 * Implements hook_eu_cookie_compliance_cid_alter().
 */
function eu_cookie_compliance_rocketship_eu_cookie_compliance_cid_alter(&$cid) {
  $routeMatch = \Drupal::routeMatch();
  $cidParts = ['route' => $routeMatch->getRouteName()] + $routeMatch->getRawParameters()->all();
  $cid .= ':' . implode(':', array_map(
    function ($v, $k) { return preg_replace('@[^a-z0-9-]+@','_', sprintf("%s_%s", $k, $v)); },
    $cidParts,
    array_keys($cidParts)
  ));
}

/**
 * Helper function to set module weight to 1 higher than EUCC.
 * Implements hook_field_formatter_info_alter().
 */
function eu_cookie_compliance_rocketship_field_formatter_info_alter(array &$info) {
  $formatters = [
    'iframe_only' => 'Drupal\eu_cookie_compliance_rocketship\Plugin\Field\FieldFormatter\CookieBlockedIframeOnly',
    'iframe_default' => 'Drupal\eu_cookie_compliance_rocketship\Plugin\Field\FieldFormatter\CookieBlockedIframe',
    'video_embed_field_video' => 'Drupal\eu_cookie_compliance_rocketship\Plugin\Field\FieldFormatter\CookieBlockedVideo',
  ];
  foreach ($formatters as $id => $class) {
    if (isset($info[$id])) {
      $info[$id]['class'] = $class;
      $info[$id]['provider'] = 'eu_cookie_compliance_rocketship';
    }
  }
}
